# Getting started

Getting started: https://squidfunk.github.io/mkdocs-material/getting-started/

## Local development

### Installation

1. Install mkdocs-material:

    ```
    python -m pip install mkdocs-material
    ```

1. `cd` to the root of this repository (folder)
1. run `mkdocs serve`

> The site should be served at  http://127.0.0.1:8000/mkdocs-test/

### Documentation development

Documentation files should be of the format `markdown` (`.md`).


## Multirepos

### mkdocs

<https://github.com/jdoiro3/mkdocs-multirepo-plugin>

### hugo

<https://discourse.gohugo.io/t/building-content-from-multiple-repositories/34636/4>

### a third apporach

To display the documentation from multiple GitLab repositories in one static site using MkDocs, you can follow these steps:

* Clone all the microservice repositories to your local machine.

* For each microservice repository, navigate to the doc/ folder and create a new MkDocs project by running mkdocs new ..

* Edit the mkdocs.yml file in each microservice repository to configure the documentation site.

* Use the mkdocs build command to build the static site for each microservice repository.

* Copy the built site files for each microservice repository to a new directory that will serve as the root directory for the merged documentation site.

* Create a new MkDocs project in the root directory by running mkdocs new . and configure the mkdocs.yml file with the appropriate navigation links to include the documentation from each microservice repository.

* Use the mkdocs serve command to build and serve the merged documentation site.

When you run the mkdocs serve command, MkDocs will merge the documentation from all the microservice repositories and display them in one static site. You can use the navigation links to switch between the different microservices' documentation. Note that this approach assumes that the documentation for each microservice is relatively small and can be easily included in a single MkDocs site. If the documentation for each microservice is large, you may want to consider using a different approach, such as hosting each microservice's documentation in a separate MkDocs site and using a search tool to search across all the sites.
